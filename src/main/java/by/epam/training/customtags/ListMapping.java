package by.epam.training.customtags;

import javax.servlet.ServletRegistration;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class ListMapping extends TagSupport {

    private static final String URL_KEY = "url";
    private static final String SERVLET_KEY = "servlet";

    private ServletRequest request;
    private Iterator<Map.Entry<String, String>> urlIterator;

    private void setAttributes(final Map.Entry<String, String> entry) {
        request.setAttribute(URL_KEY, entry.getKey());
        request.setAttribute(SERVLET_KEY, entry.getValue());
    }

    @Override
    public int doAfterBody() throws JspException {
        if (!urlIterator.hasNext()) {
            return SKIP_BODY;
        }
        setAttributes(urlIterator.next());
        return EVAL_BODY_AGAIN;
    }

    @Override
    public void setPageContext(final PageContext pageContext) {
        super.setPageContext(pageContext);

        request = pageContext.getRequest();
        Map<String, String> map = new LinkedHashMap<>();

        for (ServletRegistration servletRegistration :
                pageContext
                        .getServletContext()
                        .getServletRegistrations()
                        .values()) {
            for (String urlMapping : servletRegistration.getMappings()) {
                map.put(urlMapping, servletRegistration.getName());
            }
        }

        urlIterator = map.entrySet().iterator();
    }

    @Override
    public int doStartTag() throws JspException {
        if (urlIterator == null && !urlIterator.hasNext()) {
            return SKIP_BODY;
        }
        // Show first element
        setAttributes(urlIterator.next());
        return EVAL_BODY_INCLUDE;
    }

    @Override
    public int doEndTag() throws JspException {
        request.removeAttribute(URL_KEY);
        request.removeAttribute(SERVLET_KEY);
        return super.doEndTag();
    }
}
