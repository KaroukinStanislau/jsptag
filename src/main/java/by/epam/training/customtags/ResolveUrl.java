package by.epam.training.customtags;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class ResolveUrl extends SimpleTagSupport {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    private ServletContext getServletContext() {
        return ((PageContext) getJspContext()).getServletContext();
    }

    @Override
    public void doTag() throws JspException, IOException {
        getJspContext().setAttribute("url", url);
        getJspContext().setAttribute("resource", getServletContext().getRealPath(url));
        getJspBody().invoke(null);
    }
}
