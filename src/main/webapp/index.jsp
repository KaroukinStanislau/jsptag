<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="utils" uri="http://www.utils.tag" %>
<html>
  <head>
    <title>Jsp Tag API Task</title>
  </head>
  <body>
    <utils:listmapping>
      ${url} - ${servlet}<br/>
    </utils:listmapping>
    <br/>
    <utils:resolveurl url="resources/css/bootstrap.css">
      ${url} - ${resource}
    </utils:resolveurl>
  </body>
</html>
